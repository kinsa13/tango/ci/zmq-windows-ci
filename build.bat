echo on

pushd "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build"
call vcvarsall.bat %ARCH% -vcvars_ver=%VCVARS_VER%
popd

md build staging\lib staging\bin

pushd build
cmake -DCMAKE_INSTALL_PREFIX=../dist -DZMQ_BUILD_TESTS=0 -DENABLE_CLANG=0 -DBUILD_TESTS=0 ^
        -DENABLE_RADIX_TREE=0 -DENABLE_DRAFTS=0 -DENABLE_CURVE=0 -DENABLE_CPACK=0 -DENABLE_WS=0 ^
        -DWITH_DOCS=0 -DENABLE_PRECOMPILED=0 -DCMAKE_CXX_FLAGS_RELEASE="/MT /O2 /Ob2 /DNDEBUG" ^
        -DCMAKE_C_FLAGS_RELEASE="/MT /O2 /Ob2 /DNDEBUG" -DCMAKE_BUILD_TYPE=Release ^
        -DBUILD_SHARED=0 -G"Ninja" ../libzmq && ^
ninja install || goto :error
popd

copy dist\lib\libzmq-mt-s-4_3_4.lib staging\lib || goto :error

rd /s/q build dist
md build
pushd build
cmake -DCMAKE_INSTALL_PREFIX=../dist -DZMQ_BUILD_TESTS=0 -DENABLE_CLANG=0 -DBUILD_TESTS=0 ^
        -DENABLE_RADIX_TREE=0 -DENABLE_DRAFTS=0 -DENABLE_CURVE=0 -DENABLE_CPACK=0 -DENABLE_WS=0 ^
        -DWITH_DOCS=0 -DENABLE_PRECOMPILED=0 -DCMAKE_CXX_FLAGS_RELEASE="/MT /O2 /Ob2 /DDEBUG" ^
        -DCMAKE_C_FLAGS_RELEASE="/MT /O2 /Ob2 /DDEBUG" -DCMAKE_BUILD_TYPE=Debug ^
        -DBUILD_SHARED=0 -G"Ninja" ../libzmq && ^
ninja install || goto :error
popd

copy dist\lib\libzmq-mt-sgd-4_3_4.lib staging\lib || goto :error

rd /s/q build dist
md build
pushd build
cmake -DCMAKE_INSTALL_PREFIX=../dist -DZMQ_BUILD_TESTS=0 -DENABLE_CLANG=0 -DBUILD_TESTS=0 ^
        -DENABLE_RADIX_TREE=0 -DENABLE_DRAFTS=0 -DENABLE_CURVE=0 -DENABLE_CPACK=0 -DENABLE_WS=0 ^
        -DWITH_DOCS=0 -DENABLE_PRECOMPILED=0 -DCMAKE_CXX_FLAGS_RELEASE="/MD /O2 /Ob2 /DDEBUG" ^
        -DCMAKE_C_FLAGS_RELEASE="/MD /O2 /Ob2 /DDEBUG" -DCMAKE_BUILD_TYPE=Debug ^
        -DBUILD_SHARED=1 -G"Ninja" ../libzmq && ^
ninja install || goto :error
popd

copy dist\lib\libzmq-mt-gd-4_3_4.lib staging\lib && ^
copy dist\bin\libzmq-mt-gd-4_3_4.pdb staging\bin && ^
copy dist\bin\libzmq-mt-gd-4_3_4.dll staging\bin && ^
copy build\lib\libzmq-mt-gd-4_3_4.exp staging\lib || goto :error

rd /s/q build dist
md build
pushd build
cmake -DCMAKE_INSTALL_PREFIX=../dist -DZMQ_BUILD_TESTS=0 -DENABLE_CLANG=0 -DBUILD_TESTS=0 ^
        -DENABLE_RADIX_TREE=0 -DENABLE_DRAFTS=0 -DENABLE_CURVE=0 -DENABLE_CPACK=0 -DENABLE_WS=0 ^
        -DWITH_DOCS=0 -DENABLE_PRECOMPILED=0 -DCMAKE_CXX_FLAGS_RELEASE="/MD /O2 /Ob2 /DNDEBUG" ^
        -DCMAKE_C_FLAGS_RELEASE="/MD /O2 /Ob2 /DNDEBUG" -DCMAKE_BUILD_TYPE=Release ^
        -DBUILD_SHARED=1 -G"Ninja" ../libzmq && ^
ninja install || goto :error
popd

copy dist\lib\libzmq-mt-4_3_4.lib staging\lib && ^
copy dist\bin\libzmq-mt-4_3_4.dll staging\bin && ^
copy build\lib\libzmq-mt-4_3_4.exp staging\lib && ^
xcopy dist\include staging\include /e/y/i && ^
xcopy dist\bin staging\bin /e/y/i || goto :error

copy cppzmq\zmq.hpp staging\include && ^
copy cppzmq\zmq_addon.hpp staging\include || goto :error

md deploy
cd staging
7z a ..\deploy\%DIST_NAME% .

goto :EOF

:error
echo Failed with error #%errorlevel%.
exit /b %errorlevel%
